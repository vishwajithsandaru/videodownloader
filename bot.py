from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException as NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException as StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
import youtube_dl
from youtube_dl.utils import DownloadError as DownloadError

already_clicked = False
i = 0
url = ""
dld = ""
download_videos = False
dir_path = ""
size =0
download_videos = False

def init_site():
    global dld
    global download_videos
    global dir_path
    global url_list
    browser = webdriver.Firefox(executable_path='./geckodriver')
    browser.get('https://sso.teachable.com/secure/146684/users/sign_in')
    sleep(10)
    try_to_fill_cred(browser)
    sleep(15)
    # browser.get('https://codewithmosh.com/courses/293204/lectures/4509488')
    print("Click on the course")
    x = input("Proceed? (y/n): ")

    if x is 'y':
        dld = input("Do you want to download the videos now? (y/n): ")

        if dld is 'y':
            download_videos = True 
        else:
            print("Only the URL list will be saved.")

        dir_path = input('Enter the path of the download directory: ')
        fileurl = dir_path+"/url_list"
        url_list = open(fileurl, 'w')
        iterate_side_bar(browser)
        url_list.close()
        browser.close()
        browser.quit()
    else:
        print('Bye! Exiting...')
        browser.close()
        browser.quit()
        exit()


def try_to_fill_cred(browser):
    try:
        sleep(1)
        browser.find_element_by_id('user_email').send_keys('##')
        browser.find_element_by_id('user_password').send_keys('##')
        browser.find_element_by_name('commit').click()
    except NoSuchElementException:
        try_to_fill_cred(browser)

def iterate_side_bar(browser):
    global size
    try:
        sleep(1)
        a_list = browser.find_elements_by_xpath("//ul[@class='section-list']//li[contains(@class, 'section-item')]//a")
        # text = browser.find_element_by_xpath("//div[@class='course-mainbar']//h2[@class='section-title']").text

        size = len(a_list) 
        for a in a_list:
            global already_clicked
            already_clicked = False
            go_to_vid(browser, a)
        print("{}/{} urls grabbed.".format(i, size))
            
    except NoSuchElementException:
        iterate_side_bar(browser)


def go_to_vid(browser, a):
    global already_clicked
    global i
    global url
    global size
    global url_list
    if not already_clicked:
        a.click()
        already_clicked = True
        sleep(2)         
    
    try:
        download_btn = browser.find_element_by_xpath("//a[@class='download']")
        title_name = browser.find_element_by_xpath("//h2[@id='lecture_heading']").text
        #print(title_name)
        while url == download_btn.get_attribute('href'):
            download_btn = browser.find_element_by_xpath("//a[@class='download']")
            title_name = browser.find_element_by_xpath("//h2[@id='lecture_heading']").text
            #print(title_name)
            a.click()
            sleep(3)
            print("Equal.. Retrying")
        else:
            url = download_btn.get_attribute('href')
            i = i + 1
            print("{}/{}, url: {}".format(i, size, url))
            linetowrite = url+','+title_name+'\n'
            url_list.write(linetowrite)


    except NoSuchElementException:
        go_to_vid(browser, a)
    except StaleElementReferenceException:
        go_to_vid(browser, a)
    except RecursionError:
        print("No download button found: Skipping..")
        size = size - 1

def download_prc(url, ydl):
    try:
        ydl.download([url])
    except DownloadError:
        download_prc(url, ydl)

def vid_download():
    global dir_path
    index = 1
    fileurl = dir_path+"/url_list"

    with open(fileurl, 'r') as inp:
        urllist = inp.readlines()
    inp.close()

    for urlname in urllist:
        
        url, filename = map(str, urlname.split(', '))
        filename = filename.split('\n')[0]
        ydl_opts = {
            'format':'best',
            'outtmpl':dir_path+'/'+str(index)+' -'+filename+'.mp4'
        }

        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            download_prc(url, ydl)
            index = index + 1
            ydl_opts = {
            'format':'best',
            'outtmpl':dir_path+'/'+str(index)+' -'+filename+'.mp4'
            }

x = input('Do you want to resume a download (y/n): ')
if x is 'y':
    dir_path = input('Enter the path of the download directory (url list file should be renamed as url_list): ')
    vid_download()
else:
    init_site()
    if download_videos:
        vid_download()


